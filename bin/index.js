#!/usr/bin/env node
const program = require('commander'),
      az      = require('../lib/azlyrics'),
      ora     = require('ora'),
      spinner = ora('Loading')

program
  .version('0.1.0')
  .option('-a, --artist [artist]', 'artist name')
  .option('-s, --song [song]', 'song name')
  .parse(process.argv)

program.artist && program.song
  ? (async () => {
      spinner.start()
      try {
        const lyrics = await az.getLyrics(program.artist, program.song)
        spinner.stop()
        console.log(lyrics)
      } catch(e) {
        spinner.stop()
        throw new Error(e)
      }
    })()
  : (() => { throw new Error('No song or artist name provided') })()
