require('dotenv').config()

const musix = require('./lib/musix-api'),
      genius = require('./lib/genius-api'),
      express = require('express'),
      app = express()

app.get('/track/id/:artist/:track', async(req, res) => {
  try {
    const result = await musix
      .getTrackId(req.params.artist, req.params.track)
    console.log(result)
    res.send(`${result}`) //stringify the output
  } catch(e) {
    console.error(e)
    res.send(e)
  }
})

app.get('/track/lyrics/:artist/:track', async(req, res) => {
  try {
    const trackId = await musix
      .getTrackId(req.params.artist, req.params.track)
    const result = await musix
      .getTrackLyrics(trackId)
    req.query.extended ? res.send(result.lyrics.lyrics_body) : res.send(result)
    } catch(e) {
    console.error(e)
    res.send(e)
  }
})

app.get('/genius/song', async(req, res) => {
  try {
    const result = await genius.getSong(3039923)
    res.send(result)
  } catch(e) {
    res.send(e)
  }
})

app.listen(3000, () => console.log('server started on port 3000'))
