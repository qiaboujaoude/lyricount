const axios   = require('axios'),
      baseUrl = 'https://api.genius.com',
      token   = process.env.GENIUS_TOKEN

const options = {
  headers: {
    'Authorization' : `Bearer ${token}`
  }
}

const getSong = async id => {
  try {
    const response = await axios.get(`${baseUrl}/songs/${id}`, options)
    return response.data
  } catch(e) {
    console.log(e)
  }
}

module.exports = {
  getSong
}
