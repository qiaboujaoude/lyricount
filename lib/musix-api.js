const axios   = require('axios'),
      baseUrl = 'http://api.musixmatch.com/ws/1.1/',
      apiKey  = process.env.MUSIXMATCH_KEY

const getTrackId = async(artist, track) => {
  try {
    const response = await axios
      .get(`${baseUrl}track.search&apikey=${apiKey}?q_artist=${artist}&q_track=${track}`)
    return response.data.message.body.track_list[0].track.track_id
  } catch(e) {
    console.error(e)
  }
}

const getTrackLyrics = async(trackId) => {
  try {
    const response = await axios
      .get(`${baseUrl}track.lyrics.get&apikey=${apiKey}?track_id=${trackId}`)
    return response.data.message.body
  } catch(e) {
    console.error(e)
  }
}

module.exports.getTrackId = getTrackId
module.exports.getTrackLyrics = getTrackLyrics


