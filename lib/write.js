const fs = require('fs')

const writeArray = array => {
  const file = fs.createWriteStream(`test.txt`)
  file.on('error', err => { throw new Error(err) })
  array.forEach(e => {
    file.write(e + '\n')
  })
  file.end();
}

const writeSong = (title, lyrics) => fs.writeFile(`${title}.txt`, lyrics, 
  e => e ? (() => { throw e })() : console.log(`${title}.txt has been saved`))

module.exports = {
  writeArray,
  writeSong
}
