require('dotenv').config()
const puppeteer = require('puppeteer'),
      write = require('./write'),
      baseUrl   = 'https://www.azlyrics.com/lyrics'


const setChromiumPath = () => {
  if (process.env.ENVIRONMENT === 'local') {
    return { headless: false,
             timeout: 0,
             executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium'
           }
  } else {
    return {}
  }
}

/**
 * @method format takes a string of artist name or song name
 * removes spaces and transforms it into lower case
 * @param {string} n name of song and/or artist
 * @return {string}
 */
const format = n => n.replace(/\s/g, '').toLowerCase()

/**
 * @method setUrl takes artist and song name and creates a url
 * @param {string} artist artist name
 * @param {string} song song name
 * @return {string} a url to be visited by puppeteer
 */
const setUrl = (artist, song) => `${baseUrl}/${format(artist)}/${format(song)}.html`

/**
 * @async
 * @method getLyrics using a XPath retrieves lyrics from azlyrics page
 * @param {string} artist artist name
 * @param {string} song song name
 * @return {string} string of the song lyrics
 */
const getLyrics = async (artist, song) => {
    const browser = await puppeteer.launch({
          executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium'
    })
    const page = await browser.newPage()
    await page.goto(setUrl(artist, song))
    const xSelector = await page.$x('/html/body/div[3]/div/div[2]/div[5]')
    if (!xSelector.length) {
      await browser.close()
      throw new Error(`Song doesn't exist`)
    }
    const lyrics = await page.evaluate(e => e.textContent, xSelector[0])
    await browser.close()
    return lyrics
}

// Gets lyrics of a song provided with url
const getSongLyricsByUrl = async url => {
  const browser = await puppeteer.launch({
        executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium'
  })
  const page = await browser.newPage()
  await page.goto(url)
  const xSelector = await page.$x('/html/body/div[3]/div/div[2]/div[5]')
  if (!xSelector.length) {
    await browser.close()
    throw new Error(`Song doesn't exist`)
  }
  const lyrics = await page.evaluate(e => e.textContent, xSelector[0])
  await browser.close()
  return lyrics
}

// Gets an array of each artist url
const getAllArtistsOfPage = async () => {
  const browser = await puppeteer.launch({
    executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium'
  })
  const page = await browser.newPage()
  await page.goto('https://www.azlyrics.com/a.html', {waitUntil: 'load'})
  const artistsLinks = await page.evaluate(() => {
    const links = Array.from(document.querySelectorAll('body > div.container.main-page > div > div > a'))
    return links.map(link => link.href)
  })
  await browser.close()
  return artistsLinks
  // write.writeArray(artistsLinks)
}

// Gets an array of each song URL of a given artist
const getUrlOfArtistSongs = async () => {
  const browser = await puppeteer.launch({
    executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium'
  })
  const page = await browser.newPage()
  await page.goto('https://www.azlyrics.com/a.html', {waitUntil: 'load'})
  await page.click('body > div.container.main-page > div > div:nth-child(1) > a:nth-child(1)')
  await page.waitFor(300)
  const songUrls = await page.evaluate(() => {
    const links = Array.from(document.querySelectorAll('#listAlbum > a'))
    return links.map(link => link.href)
    // return links.map(link => [link.href, link.innerHTML])
  })
  await browser.close()
  return songUrls
}

// Goes into the first artist in a page and gets the lyrics of each song of that artist
const getAllArtistLyrics = async () => {
  const browser = await puppeteer.launch({
    executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium',
    // headless: false
  })
  const page = await browser.newPage()
  await page.goto('https://www.azlyrics.com/a.html', {waitUntil: 'load'})
  await page.click('body > div.container.main-page > div > div:nth-child(1) > a:nth-child(1)')
  await page.waitFor(300)

  const songUrls = await page.evaluate(() => {
    const links = Array.from(document.querySelectorAll('#listAlbum > a'))
    return links.map(link => link.href)
    // return links.map(link => [link.href, link.innerHTML])
  })
  // Loop over each url
  for (let i = 0; i < songUrls.length; i++) {
    if (songUrls[i]) {
      await page.goto(songUrls[i], {waitUntil: 'load'})
      await page.waitForSelector('body > div.container.main-page > div > div.col-xs-12.col-lg-8.text-center > div:nth-child(8)')
      const xSelector = await page.$x('/html/body/div[3]/div/div[2]/div[5]')
      if (!xSelector.length) {
        throw new Error(`Song doesn't exist`)
      }
      const title = await page.$eval('body > div.container.main-page > div > div.col-xs-12.col-lg-8.text-center > b', e => e.textContent)
      // console.log(title.replace(/['"]+/g, ''))
      const lyrics = await page.evaluate(e => e.textContent, xSelector[0])
      write.writeSong(title.replace(/['"]+/g, ''), `${title}\n${lyrics}`)
    }
  }
  await browser.close()
}

const getAllPages = async () => {
  const browser = await puppeteer.launch({
    executablePath: '/Applications/Chromium.app/Contents/MacOS/Chromium',
    //headless: false
  })
  const page = await browser.newPage()
  await page.goto('https://www.azlyrics.com/', {waitUntil: 'load'})
  const pages = await page.$$eval('#artists-collapse > li > div > a', e => e.map(o => o.href))
  await browser.close()
  return pages
}

module.exports = {
  getLyrics
}
